name = "Jazzrell Frian P. Abad"
age = 33
occupation = "Software Engineer"
movie = "One More Chance"
rating = 99.6

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}%")

num1 = 2
num2 = 3
num3 = 5

num2 += num3

print(num1 * num2)
print(num1 < num3)
